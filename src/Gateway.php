<?php

namespace Omnipay\ChardCryptoPay;

use Omnipay\Common\AbstractGateway;

/**
 * Chard Crypto Payment Gateway
 *
 * @link https://github.com
 */
class Gateway extends AbstractGateway
{
    /**
     * @return string
     */
    public function getName()
    {
        return 'Chard';
    }

    /**
     * @return array
     */
    public function getDefaultParameters()
    {
        return array(
            'apiKey' => ''
        );
    }

    /**
     * @return string
     */
    public function getApiKey()
    {
        return $this->getParameter('apiKey');
    }

    /**
     * @param  string $value
     * @return $this
     */
    public function setApiKey($value)
    {
        return $this->setParameter('apiKey', $value);
    }

    public function getApiSecret()
    {
        return $this->getParameter('apiSecret');
    }

    public function setApiSecret($value)
    {
        return $this->setParameter('apiSecret', $value);
    }

    /**
     * @param  array $parameters
     * @return \Omnipay\ChardCryptoPay\Message\FetchIssuersRequest
     */
    public function fetchIssuers(array $parameters = array())
    {
        return $this->createRequest('\Omnipay\ChardCryptoPay\Message\FetchIssuersRequest', $parameters);
    }

    /**
     * @param  array $parameters
     * @return \Omnipay\ChardCryptoPay\Message\FetchPaymentMethodsRequest
     */
    public function fetchPaymentMethods(array $parameters = array())
    {
        return $this->createRequest('\Omnipay\ChardCryptoPay\Message\FetchPaymentMethodsRequest', $parameters);
    }

    /**
     * @param  array $parameters
     * @return \Omnipay\ChardCryptoPay\Message\FetchTransactionRequest
     */
    public function fetchTransaction(array $parameters = array())
    {
        return $this->createRequest('\Omnipay\ChardCryptoPay\Message\FetchTransactionRequest', $parameters);
    }

    /**
     * @param  array $parameters
     * @return \Omnipay\ChardCryptoPay\Message\PurchaseRequest
     */
    public function purchase(array $parameters = array())
    {
        return $this->createRequest('\Omnipay\ChardCryptoPay\Message\PurchaseRequest', $parameters);
    }

    /**
     * @param  array $parameters
     * @return \Omnipay\ChardCryptoPay\Message\CompletePurchaseRequest
     */
    public function completePurchase(array $parameters = array())
    {
        return $this->createRequest('\Omnipay\ChardCryptoPay\Message\CompletePurchaseRequest', $parameters);
    }

    /**
     * @param  array $parameters
     * @return \Omnipay\ChardCryptoPay\Message\RefundRequest
     */
    public function refund(array $parameters = array())
    {
        return $this->createRequest('\Omnipay\ChardCryptoPay\Message\RefundRequest', $parameters);
    }

    /**
     * @param  array $parameters
     * @return \Omnipay\ChardCryptoPay\Message\CreateCustomerRequest
     */
    public function createCustomer(array $parameters = array())
    {
        return $this->createRequest('\Omnipay\ChardCryptoPay\Message\CreateCustomerRequest', $parameters);
    }

    /**
     * @param  array $parameters
     * @return \Omnipay\ChardCryptoPay\Message\UpdateCustomerRequest
     */
    public function updateCustomer(array $parameters = array())
    {
        return $this->createRequest('\Omnipay\ChardCryptoPay\Message\UpdateCustomerRequest', $parameters);
    }

    /**
     * @param  array $parameters
     * @return \Omnipay\ChardCryptoPay\Message\FetchCustomerRequest
     */
    public function fetchCustomer(array $parameters = array())
    {
        return $this->createRequest('\Omnipay\ChardCryptoPay\Message\FetchCustomerRequest', $parameters);
    }
}
