<?php

namespace Omnipay\ChardCryptoPay\Message;

abstract class AbstractRequest extends \Omnipay\Common\Message\AbstractRequest
{
    protected $endpoint = 'https://www.chards.co.uk/api/crypto-payment';

    public function getApiKey()
    {
        return $this->getParameter('apiKey');
    }

    public function setApiKey($value)
    {
        return $this->setParameter('apiKey', $value);
    }

    public function getApiSecret()
    {
        return $this->getParameter('apiSecret');
    }

    public function setApiSecret($value)
    {
        return $this->setParameter('apiSecret', $value);
    }

    public function setTransactionId($value)
    {
        return $this->setParameter('transactionId', $value);
    }

    public function getTransactionId()
    {
        return $this->getParameter('transactionId');
    }

    private function getApiSignature($data)
    {
        return hash_hmac('sha256', $data, $this->getApiSecret());
    }

    protected function sendRequest($method, $endpoint, $data = null)
    {
        $data = json_encode($data);
        $response = $this->httpClient->request(
            $method,
            $this->endpoint . $endpoint,
            array(
                'Authorization' => 'Bearer ' . $this->getApiKey(),
                'x-signature' => $this->getApiSignature($data),
                'content-type'  => 'application/json'
            ),
            $data
        );

        return json_decode($response->getBody(), true);
    }
}
